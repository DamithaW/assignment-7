#include <stdio.h>
#define M 2

int main()
{
    int mtrx1[M][M];
    int mtrx2[M][M];

    printf("Your values for first matrix1  \n\n ");
    for(int pos_x=0  ; pos_x < M ; pos_x++ )
        {
      for(int pos_y=0 ; pos_y < M ; pos_y++)
      {
        printf("The position x=%d y=%d :", pos_x, pos_y );
        scanf("%i" , &mtrx1[pos_x][pos_y]);
      }
        }


    printf("Your values for second matrix1 \n\n ");
    for(int pos_x=0  ; pos_x < M ; pos_x++ )
    {
      for(int pos_y=0 ; pos_y < M ; pos_y++)
      {
        printf("The position x=%d y=%d :", pos_x, pos_y );
        scanf("%i" , &mtrx2[pos_x][pos_y]);
      }
    }


    int result[M][M];

    int i, a, f;

    for (i=0; i < M; i++)
    {
        for (a=0; a < M; a++)
        {
            result[i][a] = 0;
            for (f= 0; f < M; f++)
                result[i][a] += mtrx1[i][f]*mtrx2[f][a];
        }
    }


    int p, q;


    printf("Final results : \n\n\n");
    for (p=0; p < M; p++)
    {
        for (q=0; q < M; q++)
           printf("%d ", mtrx1[p][q]);
        printf("\n");
    }

    printf("X\n");

    for (p=0; p < M; p++)
    {
        for (q=0; q < M; q++)
           printf("%d ", mtrx2[p][q]);
        printf("\n");
    }

    printf("=\n");

    for (p=0; p < M; p++)
    {
        for (q=0; q < M; q++)
           printf("%d ", result[p][q]);
        printf("\n");
    }

    return 0;
}
